import React from 'react';
import {
    Switch,
    Route,
} from 'react-router-dom';
import Main from '../views/Main';
import Add from '../views/Add';
import Edit from '../views/Edit';
import NotFound404 from '../views/NotFound404';
import Done from '../views/Done';

function RouterView({addItem, todo}) {
    return (
        <Switch>
          <Route path="/add" render={ ()=> <Add />  }>
          </Route>
          <Route path="/edit/:item" render={ ()=> <Edit addItem={addItem} todo={todo} />  }>
          </Route>
          <Route path="/done" render={ ()=> <Done />  }>
          </Route>
          <Route path="/" exact={true} render={ ()=> <Main />  }>
          </Route>
          <Route path="*" render={ () => <NotFound404 />}>
          </Route>
        </Switch>
    );
}

export default RouterView;