import React, { useState, useEffect, useRef } from 'react';
import './assets/scss/index.scss';
import {
  BrowserRouter as Router,
  Link
} from 'react-router-dom';
import  RouterView from './router/';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './store/reducer';


const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

function App(props) {
  const [todo, setTodo] = useState(['澆花', '拖地', '掃地', '洗衣服']);

  function addItem(item){
    setTodo([item, ...todo]);
  }

  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <nav>
            <ul>
              <li><Link to="/">Home</Link></li>
              <li><Link to="/done">Done</Link></li>
              <li><Link to="/add">Add</Link></li>
            </ul>
          </nav>

          <RouterView addItem={addItem} todo={todo} />
        </div>
      </Router>
    </Provider>
  );
}

export default App;
