import {ADD, DONE, DELETE, INIT} from './action';
 
const reducer = (state={}, action) => {
    switch (action.type) {
        case ADD:
            //放在LIST的第一個
            return {...state, todo:[action.payload, ...state.todo]};
        case DONE:
            return {...state, done: [action.payload, ...state.done]};
        case DELETE:
            const index = state.todo.indexOf(action.payload);
            const newTodo = [...state.todo];
            newTodo.splice(index, 1);
            return {...state, todo: [...newTodo]};
        case INIT:
            return {todo: [ ...action.payload ], done:[]};
        default:
            return { ...state };
    }
}

export default reducer;

/*
{
    type: 'ADD',
    payload: '澆花'
}

store = {
    list: ['澆花', '掃地', '拖地']
}
*/
