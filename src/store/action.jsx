export const ADD = 'ADD';
export const DONE = 'DONE';
export const DELETE = 'DELETE';
export const INIT = 'INIT';

export function done(item){
    return {
        type: DONE,
        payload: item
    };
}

export function deleteItem(item){
    return {
        type: DELETE,
        payload: item
    }
}

export function addItem(newItem){
    return {
        type: ADD,
        payload: newItem
    };
}

export function initial(allItems){
    return {
        type: INIT,
        payload: allItems
    }; 
}
