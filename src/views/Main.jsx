import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect, useSelector, useDispatch } from 'react-redux';
import { initial, done, deleteItem } from '../store/action';

function Main() {
    const todoList = useSelector(state => state.todo);
    const doneList = useSelector(state => state.done);
    const dispatch = useDispatch();

    const renderTodoList = todoList ? todoList.map(item => {
        if(!doneList.includes(item)){
            return(<li key={item}>
            <div>
                <Link to={`/edit/${item}`}>{item}</Link>
            </div>

            <div onClick={()=>{
                dispatch(done(item));
            }}>
                Done
            </div>
            <div onClick={()=>{
                dispatch(deleteItem(item));
            }}>
                Delete
            </div>
            </li>);
        }
        return null;
    }) : (null);
    
    useEffect(() => {
        if(!todoList){
            const allItems = ['澆花', '拖地', '掃地', '洗衣服', '烘衣服'];
            dispatch(initial(allItems));
        }
    }, [initial, todoList, dispatch]);

    return (
        <div>
            <ul className="todoList">{renderTodoList}</ul>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        // todoList: state.todo
        // doneList: state.done
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        // initial: allItems => {
        //     dispatch(initial(allItems));
        // },
        // done: (item) => {
        //     dispatch(done(item));
        // },
        // deleteItem: (item) => {
        //     dispatch(deleteItem(item));
        // }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);