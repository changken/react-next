import React from 'react';
import { Link } from 'react-router-dom';

function NotFound404(props) {
    return (
        <div>
            <h1>404 此頁面不存在</h1>
            <button><Link to="/">回到首頁!</Link></button>
        </div>
    );
}

export default NotFound404;