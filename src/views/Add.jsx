import React, { useState, useEffect } from 'react';
import {Switch, Route} from 'react-router-dom';
import NotFound404 from './NotFound404';
import { connect } from 'react-redux';
import { addItem } from '../store/action';

function Add({addItem}) {
    const [newItem, setNewItem] = useState('');

    function handleClick(event){
        event.preventDefault();
        addItem(newItem); 
        // dispatch({
        //     type: ADD,
        //     payload: newItem
        // })
        setNewItem('');
    }

    function handleChange(event){
        setNewItem(event.target.value);
    }

    return (
        <form>
            <label htmlFor="new">
                新增代辦事項:
                <input onChange={handleChange} name="new" type="text" value={newItem} />
            </label>

            <button onClick={handleClick}>新增</button>

            <Switch>
                <Route path="/add/test">
                    <NotFound404 />
                </Route>
            </Switch>
        </form>
    );
}

const mapDispatchToProps = dispatch => {
    return {
      addItem: newItem => {
        dispatch(addItem(newItem));
      }
        // dispatch: dispatch
    }
  };

export default connect(null, mapDispatchToProps)(Add);