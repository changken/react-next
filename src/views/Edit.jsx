import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

function Edit({addItem, todo}) {
    const [newItem, setNewItem] = useState('');
    const {item} = useParams();

    useEffect(()=>{
        const foucsItem = todo.filter(i => i === item)[0];
        setNewItem(foucsItem);
    },[item, todo]);

    function handleClick(event){
        event.preventDefault();
        addItem(newItem); 
    }

    function handleChange(event){
        setNewItem(event.target.value);
    }

    return (
        <form>
            <label htmlFor="new">
                修改代辦事項:
                <input onChange={handleChange} name="new" type="text" value={newItem} />
            </label>

            <button onClick={handleClick}>修改</button>
        </form>
    );
}

export default Edit;