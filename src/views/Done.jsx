import React from 'react';
import { connect, useSelector, useDispatch } from 'react-redux';
import { deleteItem } from '../store/action';

function Done() {
    const doneList = useSelector(state => state.done);
    const dispatch = useDispatch();
    
    const renderDoneList = doneList 
    ? doneList.map(item => (
        <li key={item}>
            <div>
                {item}
            </div>
            <div onClick={()=>{
                dispatch(deleteItem(item));
            }}>
                Delete
            </div>
        </li>
    )) : (null);

    return (
        <div>
            <ul className="doneList">{renderDoneList}</ul>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        // doneList: state.done
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        // deleteItem: item => {
        //     dispatch(deleteItem(item));
        // }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Done);